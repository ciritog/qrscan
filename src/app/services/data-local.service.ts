import { Injectable } from '@angular/core';
import { Registro } from '../models/registro.model';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { File } from '@ionic-native/file/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  // Array en donde se guardaran los registros
  guardados: Registro[] = [];

  constructor( private storage: Storage,
               private navCtrl: NavController,
               private iab: InAppBrowser,
               private file: File,
               private emailComposer: EmailComposer ) {

    // Obtener registros desde el storage
    storage.get('registros')
      .then( (regs:Registro[]) => {
       this.guardados = regs || [];
      });
  }

  guardarRegistro( format:string, text:string ){

    // Instanciar nuevo registro 
    const nuevoRegistro = new Registro( format, text );
    // Guardar el registro en el array, siempre en 1ra posicion
    this.guardados.unshift( nuevoRegistro );

    // Guardar registros en el storage
    this.storage.set('registros', this.guardados);

    // Abrir navegador
    this.abrirRegistro( nuevoRegistro );

    console.log(this.guardados);

  }

  abrirRegistro( registro: Registro ){

    this.navCtrl.navigateForward('/tabs/tab2');
    
    switch ( registro.type ) {
      case 'http':
        // Abris el navegador
        this.iab.create( registro.text, '_system' );
        break;
      case 'geo':
        this.navCtrl.navigateForward(`/tabs/tab2/mapa/${ registro.text }`);
        break;
    }

  }

  enviarCorreo(){

    const arrTemp = [];
    const titulos = "Tipo, Formato, Creado en, Texto\n";

    this.guardados.forEach( registro => {

      const linea = `${ registro.type }, ${ registro.format }, ${ registro.created }, ${ registro.text.replace(',', ' ') }\n`;

      arrTemp.push( linea );

    } ); 

    this.crearArchivoFisico(  arrTemp.join('') );

  }

  crearArchivoFisico( text: string ){

    this.file.checkFile(this.file.dataDirectory, 'registro.csv')
      .then( existe => {
        console.log("Existe archivo?", existe);
        this.escribirEnArchivo( text );
      } )
      .catch(err => {
        return this.file.createFile( this.file.dataDirectory, 'registros.csv', false )
          .then( creado => this.escribirEnArchivo( text ))
          .catch( err2 => console.log("No se pudo crear el archivo", err2));
      });

  }

  escribirEnArchivo( text:string ){
    this.file.writeExistingFile( this.file.dataDirectory, 'registros.csv', text );
    console.log("Archivo creado.");
    console.log(this.file.dataDirectory + 'registros.csv');

    const archivo = `${this.file.dataDirectory}/registros.csv`;

    const email = {
      to: 'soycirog@gmail.com',
      attachments: [
        archivo
      ],
      subject: 'Backup archivos',
      body: 'Te dejo el archivo',
      isHtml: true
    };
    
    // Send a text message using default options
    this.emailComposer.open(email);

  }

}
